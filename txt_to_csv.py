import xlsxwriter
import os

filename = input("Enter filename:")
base_dir = ""
source = os.path.join(base_dir, filename + '.txt')
dest = os.path.join(base_dir, filename + '.xlsx')

with open(source, 'r') as in_file:
    lines = [line.split() for line in in_file]

    with xlsxwriter.Workbook(dest) as wb:
        worksheet = wb.add_worksheet()

        for row, data in enumerate(lines):
            worksheet.write_row(row, 0, data)